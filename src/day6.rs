use std::fs;
use std::collections::HashSet;

fn find_distinct_characters(stream: &str, num: usize) -> Option<usize> {
    let bytes = stream.as_bytes();
    for i in num..bytes.len() {
        let segment: HashSet<&u8> = bytes[(i-num)..i].iter().collect();
        if segment.len() == num {
            return Some(i);
        }
    }
    None
}

pub fn find_start_of_packet(stream: &str) -> Option<usize> {
    find_distinct_characters(stream, 4)
}

pub fn find_start_of_message(stream: &str) -> Option<usize> {
    find_distinct_characters(stream, 14)
}

pub fn p1() {
    println!("{}", find_start_of_packet(&fs::read_to_string("data/day6.txt").unwrap()).unwrap());
}

pub fn p2() {
    println!("{}", find_start_of_message(&fs::read_to_string("data/day6.txt").unwrap()).unwrap());
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_find_start_of_packet() {
        assert_eq!(find_start_of_packet(""), None);
        assert_eq!(find_start_of_packet("abc"), None);
        assert_eq!(find_start_of_packet("mjqjpqmgbljsphdztnvjfqwrcgsmlb"), Some(7));
        assert_eq!(find_start_of_packet("bvwbjplbgvbhsrlpgdmjqwftvncz"), Some(5));
        assert_eq!(find_start_of_packet("nppdvjthqldpwncqszvftbrmjlhg"), Some(6));
        assert_eq!(find_start_of_packet("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"), Some(10));
        assert_eq!(find_start_of_packet("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"), Some(11));
    }

    #[test]
    fn test_find_start_of_message() {
        assert_eq!(find_start_of_message(""), None);
        assert_eq!(find_start_of_message("abc"), None);
        assert_eq!(find_start_of_message("mjqjpqmgbljsphdztnvjfqwrcgsmlb"), Some(19));
        assert_eq!(find_start_of_message("bvwbjplbgvbhsrlpgdmjqwftvncz"), Some(23));
        assert_eq!(find_start_of_message("nppdvjthqldpwncqszvftbrmjlhg"), Some(23));
        assert_eq!(find_start_of_message("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"), Some(29));
        assert_eq!(find_start_of_message("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"), Some(26));
    }
}
