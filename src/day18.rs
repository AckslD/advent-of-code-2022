use std::{collections::VecDeque, fs};
use std::fmt;
use std::collections::{HashMap, HashSet};
use std::str::FromStr;

use itertools::iproduct;

use self::Direction::*;

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy, PartialOrd, Ord)]
struct Pos {
    x: i32,
    y: i32,
    z: i32,
}

impl Pos {
    fn towards(&self, direction: &Direction) -> Self {
        let mut new_pos = self.clone();
        match direction {
            Up => {new_pos.z += 1;}
            Down => {new_pos.z -= 1;}
            East => {new_pos.x += 1;}
            West => {new_pos.x -= 1;}
            North => {new_pos.y += 1;}
            South => {new_pos.y -= 1;}
        }
        new_pos
    }

    fn neighbors(&self) -> Vec<Pos> {
        iproduct!(-1..=1, -1..=1, -1..=1)
            .map(|(xd, yd, zd)| Pos{x: self.x + xd, y: self.y + yd, z: self.z + zd})
            .filter(|p| p != self)
            .collect()
    }
}

impl fmt::Display for Pos {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {}, {})", self.x, self.y, self.z)
    }
}

impl FromStr for Pos {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let numbers: Vec<i32> = s.split(',')
            .map(|n| n.parse().unwrap())
            .collect();
        Ok(Pos{x: numbers[0], y: numbers[1], z: numbers[2]})
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
enum Direction {
    Up,
    Down,
    East,
    West,
    North,
    South,
}

impl Direction {
    fn iterator() -> impl Iterator<Item=Direction> {
        [
            Up,
            Down,
            East,
            West,
            North,
            South,
        ].iter().copied()
    }

    fn opposite(&self) -> Self {
        match self {
            Up => Down,
            Down => Up,
            East => West,
            West => East,
            North => South,
            South => North,
        }
    }
}

#[derive(Debug)]
struct Cube {
    is_inside: HashSet<Direction>,
}

impl Cube {
    fn new() -> Self {
        Self{
            is_inside: HashSet::new()
        }
    }

    fn outsides(&self) -> Vec<Direction> {
        Direction::iterator()
            .filter(|d| !self.is_inside.contains(d))
            .collect()
    }
}

fn get_cubes() -> HashMap<Pos, Cube> {
    let points: Vec<Pos> = fs::read_to_string("data/day18.txt")
        .unwrap()
        .split_terminator('\n')
        .map(|line| line.parse().unwrap())
        .collect();
    cubes_from_points(&points)
}

fn cubes_from_points(points: &Vec<Pos>) -> HashMap<Pos, Cube> {
    let mut cubes: HashMap<Pos, Cube> = HashMap::new();
    for point in points {
        let mut cube = Cube::new();
        for direction in Direction::iterator() {
            let other_point = point.towards(&direction);
            match cubes.get_mut(&other_point) {
                Some(other_cube) => {
                    cube.is_inside.insert(direction);
                    other_cube.is_inside.insert(direction.opposite());
                }
                None => {}
            }
        }
        cubes.insert(*point, cube);
    }

    cubes
}

pub fn p1() {
    let cubes = get_cubes();

    let surface_area: usize = cubes.values()
        .map(|c| 6 - c.is_inside.len())
        .sum();
    println!("{}", surface_area);
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
struct Side {
    pos: Pos,
    direction: Direction,
}

impl Side {
    fn is_incident(&self, other: &Self) -> bool {
        let self_ref = self.reference_position();
        let other_ref = other.reference_position();
        let distance = (
            (self_ref.0 - other_ref.0).powf(2.)
            + (self_ref.1 - other_ref.1).powf(2.)
            + (self_ref.2 - other_ref.2).powf(2.)
        ).sqrt();
        if self.direction == other.direction {
            distance <= 1.
        } else {
            distance <= (0.5_f32).sqrt()
        }
    }

    fn reference_position(&self) -> (f32, f32, f32) {
        let neighbor = self.pos.towards(&self.direction);
        (
            (self.pos.x + neighbor.x) as f32 / 2.,
            (self.pos.y + neighbor.y) as f32 / 2.,
            (self.pos.z + neighbor.z) as f32 / 2.,
        )
    }
}

type Sides = HashSet<Side>;
type Cubes = HashMap<Pos, Cube>;

fn side_neighbors(side: &Side, cubes: &Cubes) -> Vec<Side> {
    let mut neighbors = Vec::new();

    for pos in side.pos.neighbors().iter().chain([side.pos].iter()) {
        let maybe_cube = cubes.get(&pos);
        if maybe_cube.is_none() {
            continue;
        }
        let cube = maybe_cube.unwrap();
        for direction in cube.outsides() {
            if *pos == side.pos && direction == side.direction {
                continue;
            }
            if *pos == side.pos && cubes.contains_key(&pos.towards(&direction).towards(&side.direction)) {
                continue;
            }
            let other_side = Side{pos: *pos, direction};
            if side.is_incident(&other_side) {
                neighbors.push(other_side);
            }
        }
    }
    neighbors
}

fn bfs(start: &Side, cubes: &Cubes) -> HashSet<Side> {
    let mut queue: VecDeque<Side> = VecDeque::new();
    queue.push_back(*start);
    let mut visited: HashSet<Side> = HashSet::new();
    visited.insert(*start);
    while !queue.is_empty() {
        let side = queue.pop_front().unwrap();
        for neighbor in side_neighbors(&side, cubes) {
            if visited.contains(&neighbor) {
                continue;
            }
            visited.insert(neighbor);
            queue.push_back(neighbor);
        }
    }
    visited
}

fn is_blocked_towards(side: &Side, cubes: &Cubes, limits: &Limits) -> bool {
    let mut pos = side.pos;
    while limits.contains(&pos) {
        pos = pos.towards(&side.direction);
        if cubes.contains_key(&pos) {
            return true;
        }
    }
    false
}

struct Limits {
    min: Pos,
    max: Pos,
}

impl Limits {
    fn new(cubes: &Cubes) -> Self {
        Self{
            min: Pos{
                x: cubes.keys().map(|p| p.x).min().unwrap(),
                y: cubes.keys().map(|p| p.y).min().unwrap(),
                z: cubes.keys().map(|p| p.z).min().unwrap(),
            },
            max: Pos{
                x: cubes.keys().map(|p| p.x).max().unwrap(),
                y: cubes.keys().map(|p| p.y).max().unwrap(),
                z: cubes.keys().map(|p| p.z).max().unwrap(),
            },
        }
    }

    fn contains(&self, pos: &Pos) -> bool {
        self.min.x <= pos.x && pos.x <= self.max.x
        && self.min.y <= pos.y && pos.y <= self.max.y
        && self.min.z <= pos.z && pos.z <= self.max.z
    }
}

fn get_side_clusters(cubes: &Cubes) -> Vec<HashSet<Side>> {
    let mut sides: Sides = HashSet::new();
    for (pos, cube) in cubes.iter() {
        for direction in cube.outsides() {
            sides.insert(Side{pos: *pos, direction});
        }
    }
    let mut clusters = Vec::new();
    loop {
        match sides.iter().next().copied() {
            Some(side) => {
                let visited = bfs(&side, cubes);
                for other in visited.iter() {
                    sides.remove(&other);
                }
                clusters.push(visited);
            }
            None => {break;}
        }
    }
    clusters
}

fn is_internal_cluster(cluster: &HashSet<Side>, cubes: &Cubes, limits: &Limits) -> bool {
    cluster.iter().all(|s| is_blocked_towards(s, &cubes, &limits))
}

pub fn p2() {
    let cubes = get_cubes();
    let clusters = get_side_clusters(&cubes);
    let limits = Limits::new(&cubes);
    let internal_clusters: Vec<Sides> = clusters.iter().filter(|cluster| is_internal_cluster(cluster, &cubes, &limits)).cloned().collect();
    let surface_area: usize = clusters.iter().map(|cluster| cluster.len()).sum::<usize>() - internal_clusters.iter().map(|cluster| cluster.len()).sum::<usize>();
    println!("external surface area: {}", surface_area);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_neighbors() {
        assert_eq!(Pos{x: 0, y: 0, z: 0}.neighbors().len(), 26);
    }

    macro_rules! cubes_from_points_tests {
        ($($name:ident: $value:expr,)*) => {
            $(
                #[test]
                fn $name() {
                    let (points, expected) = $value;
                    let cubes = cubes_from_points(&points);
                    let expected_cubes: Cubes = expected.into_iter().collect();
                    assert_eq!(cubes.len(), expected_cubes.len());
                    for (pos, cube) in cubes {
                        assert!(expected_cubes.contains_key(&pos));
                        assert_eq!(cube.is_inside, expected_cubes[&pos].is_inside);
                    }
                }
            )*
        }
    }

    cubes_from_points_tests! {
        test_cubes_from_points_0: (vec![
            Pos{x: 0, y: 0, z: 0},
        ], vec![
            (Pos{x: 0, y: 0, z: 0}, Cube{is_inside: HashSet::new()}),
        ]),
        test_cubes_from_points_1: (vec![
            Pos{x: 0, y: 0, z: 0},
            Pos{x: 1, y: 0, z: 0},
        ], vec![
            (Pos{x: 0, y: 0, z: 0}, Cube{is_inside: [East].into_iter().collect()}),
            (Pos{x: 1, y: 0, z: 0}, Cube{is_inside: [West].into_iter().collect()}),
        ]),
    }

    macro_rules! is_incident_tests {
        ($($name:ident: $value:expr,)*) => {
            $(
                #[test]
                fn $name() {
                    let (side, other, expected) = $value;
                    assert_eq!(side.is_incident(&other), expected);
                }
            )*
        }
    }

    is_incident_tests! {
        test_is_incident_0: (
            Side{pos: Pos{x: 0, y: 0, z: 0}, direction: East},
            Side{pos: Pos{x: 0, y: 0, z: 0}, direction: South},
            true,
        ),
        test_is_incident_1: (
            Side{pos: Pos{x: 0, y: 0, z: 0}, direction: East},
            Side{pos: Pos{x: 0, y: 0, z: 0}, direction: West},
            false,
        ),
        test_is_incident_2: (
            Side{pos: Pos{x: 0, y: 0, z: 0}, direction: Up},
            Side{pos: Pos{x: 0, y: 0, z: 0}, direction: Down},
            false,
        ),
        test_is_incident_3: (
            Side{pos: Pos{x: 0, y: 0, z: 0}, direction: Up},
            Side{pos: Pos{x: 1, y: 0, z: 0}, direction: Up},
            true,
        ),
    }

    macro_rules! side_neighbors_tests {
        ($($name:ident: $value:expr,)*) => {
            $(
                #[test]
                fn $name() {
                    let (points, direction, expected) = $value;
                    let cubes = cubes_from_points(&points);
                    let side = &Side{pos: points[0], direction};
                    let got: HashSet<Side> = side_neighbors(&side, &cubes).into_iter().collect();
                    println!("{:?}", got.symmetric_difference(&expected).collect::<HashSet<&Side>>());
                    assert_eq!(got, expected);
                }
            )*
        }
    }

    side_neighbors_tests! {
        test_side_neighbors_0: (vec![Pos{x: 0, y: 0, z: 0}], Up, [
            Side{pos: Pos{x: 0, y: 0, z: 0}, direction: East},
            Side{pos: Pos{x: 0, y: 0, z: 0}, direction: West},
            Side{pos: Pos{x: 0, y: 0, z: 0}, direction: North},
            Side{pos: Pos{x: 0, y: 0, z: 0}, direction: South},
        ].into_iter().collect::<HashSet<Side>>()),
        test_side_neighbors_1: (vec![
            Pos{x: 0, y: 0, z: 0},
            Pos{x: 1, y: 0, z: 0},
        ], Up, [
            Side{pos: Pos{x: 0, y: 0, z: 0}, direction: West},
            Side{pos: Pos{x: 0, y: 0, z: 0}, direction: North},
            Side{pos: Pos{x: 0, y: 0, z: 0}, direction: South},
            Side{pos: Pos{x: 1, y: 0, z: 0}, direction: Up},
        ].into_iter().collect::<HashSet<Side>>()),
        test_side_neighbors_2: (vec![
            Pos{x: 0, y: 0, z: 0},
            Pos{x: 1, y: 0, z: 1},
        ], East, [
            Side{pos: Pos{x: 0, y: 0, z: 0}, direction: Down},
            Side{pos: Pos{x: 0, y: 0, z: 0}, direction: North},
            Side{pos: Pos{x: 0, y: 0, z: 0}, direction: South},
            Side{pos: Pos{x: 1, y: 0, z: 1}, direction: Down},
        ].into_iter().collect::<HashSet<Side>>()),
    }

    macro_rules! bfs_tests {
        ($($name:ident: $value:expr,)*) => {
            $(
                #[test]
                fn $name() {
                    let (points, direction, expected) = $value;
                    let cubes = cubes_from_points(&points);
                    let side = &Side{pos: points[0], direction};
                    assert_eq!(bfs(&side, &cubes).len(), expected);
                }
            )*
        }
    }

    bfs_tests! {
        test_bfs_0: (vec![Pos{x: 0, y: 0, z: 0}], Up, 6),
        test_bfs_1: (vec![
            Pos{x: 0, y: 0, z: 0},
            Pos{x: 1, y: 0, z: 0},
        ], Up, 10),
    }

    macro_rules! get_side_clusters_tests {
        ($($name:ident: $value:expr,)*) => {
            $(
                #[test]
                fn $name() {
                    let (points, mut expected) = $value;
                    let cubes = cubes_from_points(&points);
                    let mut clusters: Vec<Sides> = get_side_clusters(&cubes);
                    clusters.sort_by(|a, b| (a.len(), a.iter().next().unwrap().pos).cmp(&(b.len(), b.iter().next().unwrap().pos)));
                    expected.sort_by(|a, b| (a.len(), a.iter().next().unwrap().pos).cmp(&(b.len(), b.iter().next().unwrap().pos)));
                    assert_eq!(clusters, expected);
                }
            )*
        }
    }

    get_side_clusters_tests! {
        test_side_clusters_0: (vec![
            Pos{x: 0, y: 0, z: 0},
        ], vec![
            [
                Side{pos: Pos{x: 0, y: 0, z: 0}, direction: West},
                Side{pos: Pos{x: 0, y: 0, z: 0}, direction: East},
                Side{pos: Pos{x: 0, y: 0, z: 0}, direction: North},
                Side{pos: Pos{x: 0, y: 0, z: 0}, direction: South},
                Side{pos: Pos{x: 0, y: 0, z: 0}, direction: Up},
                Side{pos: Pos{x: 0, y: 0, z: 0}, direction: Down},
            ].into_iter().collect::<Sides>(),
        ]),
        test_side_clusters_1: (vec![
            Pos{x: 0, y: 0, z: 0},
            Pos{x: 1, y: 0, z: 0},
        ], vec![
            [
                Side{pos: Pos{x: 0, y: 0, z: 0}, direction: West},
                Side{pos: Pos{x: 0, y: 0, z: 0}, direction: North},
                Side{pos: Pos{x: 0, y: 0, z: 0}, direction: South},
                Side{pos: Pos{x: 0, y: 0, z: 0}, direction: Up},
                Side{pos: Pos{x: 0, y: 0, z: 0}, direction: Down},
                Side{pos: Pos{x: 1, y: 0, z: 0}, direction: East},
                Side{pos: Pos{x: 1, y: 0, z: 0}, direction: North},
                Side{pos: Pos{x: 1, y: 0, z: 0}, direction: South},
                Side{pos: Pos{x: 1, y: 0, z: 0}, direction: Up},
                Side{pos: Pos{x: 1, y: 0, z: 0}, direction: Down},
            ].into_iter().collect::<Sides>(),
        ]),
        test_side_clusters_2: (vec![
            Pos{x: 0, y: 0, z: 0},
            Pos{x: 2, y: 0, z: 0},
        ], vec![
            [
                Side{pos: Pos{x: 0, y: 0, z: 0}, direction: West},
                Side{pos: Pos{x: 0, y: 0, z: 0}, direction: East},
                Side{pos: Pos{x: 0, y: 0, z: 0}, direction: North},
                Side{pos: Pos{x: 0, y: 0, z: 0}, direction: South},
                Side{pos: Pos{x: 0, y: 0, z: 0}, direction: Up},
                Side{pos: Pos{x: 0, y: 0, z: 0}, direction: Down},
            ].into_iter().collect::<Sides>(),
            [
                Side{pos: Pos{x: 2, y: 0, z: 0}, direction: West},
                Side{pos: Pos{x: 2, y: 0, z: 0}, direction: East},
                Side{pos: Pos{x: 2, y: 0, z: 0}, direction: North},
                Side{pos: Pos{x: 2, y: 0, z: 0}, direction: South},
                Side{pos: Pos{x: 2, y: 0, z: 0}, direction: Up},
                Side{pos: Pos{x: 2, y: 0, z: 0}, direction: Down},
            ].into_iter().collect::<Sides>(),
        ]),
    }

    macro_rules! internal_clusters_tests {
        ($($name:ident: $value:expr,)*) => {
            $(
                #[test]
                fn $name() {
                    let (points, expected) = $value;
                    let cubes = cubes_from_points(&points);
                    let limits = Limits::new(&cubes);
                    let mut clusters: Vec<Sides> = get_side_clusters(&cubes);
                    clusters.sort_by(|a, b| (a.len(), a.iter().next().unwrap().pos).cmp(&(b.len(), b.iter().next().unwrap().pos)));
                    let is_internal: Vec<bool> = clusters.iter().map(|cluster| is_internal_cluster(cluster, &cubes, &limits)).collect();
                    assert_eq!(is_internal, expected);
                }
            )*
        }
    }

    internal_clusters_tests! {
        test_internal_clusters_3: (vec![
            Pos{x: 0, y: 0, z: 0},
            Pos{x: 1, y: 0, z: 0},
            Pos{x: 2, y: 0, z: 0},
            Pos{x: 0, y: 1, z: 0},
            Pos{x: 1, y: 1, z: 0},
            Pos{x: 2, y: 1, z: 0},
            Pos{x: 0, y: 2, z: 0},
            Pos{x: 1, y: 2, z: 0},
            Pos{x: 2, y: 2, z: 0},

            Pos{x: 0, y: 0, z: 1},
            Pos{x: 1, y: 0, z: 1},
            Pos{x: 2, y: 0, z: 1},
            Pos{x: 0, y: 1, z: 1},
            // Pos{x: 1, y: 1, z: 1},
            Pos{x: 2, y: 1, z: 1},
            Pos{x: 0, y: 2, z: 1},
            Pos{x: 1, y: 2, z: 1},
            Pos{x: 2, y: 2, z: 1},

            Pos{x: 0, y: 0, z: 2},
            Pos{x: 1, y: 0, z: 2},
            Pos{x: 2, y: 0, z: 2},
            Pos{x: 0, y: 1, z: 2},
            Pos{x: 1, y: 1, z: 2},
            Pos{x: 2, y: 1, z: 2},
            Pos{x: 0, y: 2, z: 2},
            Pos{x: 1, y: 2, z: 2},
            Pos{x: 2, y: 2, z: 2},
        ], vec![
            true,
            false,
        ]),
        test_internal_clusters_4: (vec![
            Pos{x: 0, y: 0, z: 0},
            // Pos{x: 1, y: 0, z: 0},
            Pos{x: 2, y: 0, z: 0},
            Pos{x: 0, y: 1, z: 0},
            Pos{x: 1, y: 1, z: 0},
            Pos{x: 2, y: 1, z: 0},
            Pos{x: 0, y: 2, z: 0},
            Pos{x: 1, y: 2, z: 0},
            Pos{x: 2, y: 2, z: 0},

            Pos{x: 0, y: 0, z: 1},
            Pos{x: 1, y: 0, z: 1},
            Pos{x: 2, y: 0, z: 1},
            Pos{x: 0, y: 1, z: 1},
            // Pos{x: 1, y: 1, z: 1},
            Pos{x: 2, y: 1, z: 1},
            Pos{x: 0, y: 2, z: 1},
            Pos{x: 1, y: 2, z: 1},
            Pos{x: 2, y: 2, z: 1},

            Pos{x: 0, y: 0, z: 2},
            Pos{x: 1, y: 0, z: 2},
            Pos{x: 2, y: 0, z: 2},
            Pos{x: 0, y: 1, z: 2},
            Pos{x: 1, y: 1, z: 2},
            Pos{x: 2, y: 1, z: 2},
            Pos{x: 0, y: 2, z: 2},
            Pos{x: 1, y: 2, z: 2},
            Pos{x: 2, y: 2, z: 2},
        ], vec![
            true,
            false,
        ]),
    }
}
