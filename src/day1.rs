use std::fs;

fn iter_calorie_count() -> Vec<u32> {
    fs::read_to_string("data/day1.txt")
        .expect("Should be able to read file")
        .split("\n\n")
        .map(|group| {
            group.split_terminator('\n')
                .map(|line| line.parse::<u32>().expect("Should be able to parse to int"))
                .sum()
        })
        .collect()
}

pub fn p1() {
    let max_calories: u32 = *iter_calorie_count().iter().max().expect("Should have at least one elf");
    println!("{}", max_calories);
}

pub fn p2() {
    let mut counts = iter_calorie_count();
    // reverse sort
    counts.sort_by(|a, b| b.cmp(a));
    let top_three: u32 = counts.into_iter().take(3).sum();
    println!("{}", top_three);
}
