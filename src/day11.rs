use std::cell::RefCell;
use std::collections::VecDeque;
use std::collections::HashSet;

struct Monkey {
    items: VecDeque<u64>,
    operation: Box<dyn Fn(u64) -> u64>,
    test: Box<dyn Fn(u64) -> usize>,
    divisor: u64,
    n_inspections: u64,
}

macro_rules! monkey {
    (
        Starting items: $($i:literal),+;
        Operation: new = $old:ident $op:tt $op_other:expr;
        Test: divisible by $div:literal;
            If true: throw to monkey $tm:literal;
            If false: throw to monkey $fm:literal;
    ) => {
        Monkey{
            items: VecDeque::from([$($i),+]),
            // operation: Box::new(|old| {let $old = old; println!("{}x{}", $old, $op_other); $old $op $op_other}),
            operation: Box::new(|old| {let $old = old; $old $op $op_other}),
            test: Box::new(|old| if old % $div == 0 {$tm} else {$fm}),
            divisor: $div,
            n_inspections: 0,
        }
    };
}

impl Monkey {
    fn next(&mut self, post_op: impl Fn(u64) -> u64) -> Option<(usize, u64)> {
        let mut value = self.items.pop_front()?; // TODO is this pop first?
        // println!("value={}", value);
        value = (self.operation)(value);
        // println!("value after op={}", value);
        value = post_op(value);
        // println!("value after post={}", value);
        self.n_inspections += 1;
        Some(((self.test)(value), value))
    }
}

fn make_monkeys() -> Vec<RefCell<Monkey>> {
    let monkeys: Vec<Monkey> = vec![
        // monkey!(
        //   Starting items: 79, 98;
        //   Operation: new = old * 19;
        //   Test: divisible by 23;
        //     If true: throw to monkey 2;
        //     If false: throw to monkey 3;
        // ),
        // monkey!(
        //   Starting items: 54, 65, 75, 74;
        //   Operation: new = old + 6;
        //   Test: divisible by 19;
        //     If true: throw to monkey 2;
        //     If false: throw to monkey 0;
        // ),
        // monkey!(
        //   Starting items: 79, 60, 97;
        //   Operation: new = old * old;
        //   Test: divisible by 13;
        //     If true: throw to monkey 1;
        //     If false: throw to monkey 3;
        // ),
        // monkey!(
        //   Starting items: 74;
        //   Operation: new = old + 3;
        //   Test: divisible by 17;
        //     If true: throw to monkey 0;
        //     If false: throw to monkey 1;
        // ),

        monkey!(
          Starting items: 54, 61, 97, 63, 74;
          Operation: new = old * 7;
          Test: divisible by 17;
            If true: throw to monkey 5;
            If false: throw to monkey 3;
        ),
        monkey!(
          Starting items: 61, 70, 97, 64, 99, 83, 52, 87;
          Operation: new = old + 8;
          Test: divisible by 2;
            If true: throw to monkey 7;
            If false: throw to monkey 6;
        ),
        monkey!(
          Starting items: 60, 67, 80, 65;
          Operation: new = old * 13;
          Test: divisible by 5;
            If true: throw to monkey 1;
            If false: throw to monkey 6;
        ),
        monkey!(
          Starting items: 61, 70, 76, 69, 82, 56;
          Operation: new = old + 7;
          Test: divisible by 3;
            If true: throw to monkey 5;
            If false: throw to monkey 2;
        ),
        monkey!(
          Starting items: 79, 98;
          Operation: new = old + 2;
          Test: divisible by 7;
            If true: throw to monkey 0;
            If false: throw to monkey 3;
        ),
        monkey!(
          Starting items: 72, 79, 55;
          Operation: new = old + 1;
          Test: divisible by 13;
            If true: throw to monkey 2;
            If false: throw to monkey 1;
        ),
        monkey!(
          Starting items: 63;
          Operation: new = old + 4;
          Test: divisible by 19;
            If true: throw to monkey 7;
            If false: throw to monkey 4;
        ),
        monkey!(
          Starting items: 72, 51, 93, 63, 80, 86, 81;
          Operation: new = old * old;
          Test: divisible by 11;
            If true: throw to monkey 0;
            If false: throw to monkey 4;
        ),
    ];

    let mut monkey_refs: Vec<RefCell<Monkey>> = Vec::new();
    for monkey in monkeys {
        monkey_refs.push(RefCell::new(monkey));
    }

    monkey_refs
}

fn compute_monkey_business(monkey_refs: &Vec<RefCell<Monkey>>, n_rounds: usize, post_op: impl Fn(u64) -> u64) -> u64 {

    for _round in 0..n_rounds {

        // println!("round: {}", round);
        // for monkey_ref in monkey_refs {
        //     println!("{:?}", monkey_ref.borrow().items);
        // }

        for monkey_ref in monkey_refs {
            loop {
                let throw = monkey_ref.borrow_mut().next(&post_op);
                match throw {
                    Some((idx, val)) => {monkey_refs[idx].borrow_mut().items.push_back(val);},
                    None => {break;}
                }
            }
        }
    }

    let mut n_inspections: Vec<u64> = monkey_refs.iter()
        .map(|monkey_ref| monkey_ref.borrow().n_inspections)
        .collect();
    n_inspections.sort_by(|a, b| b.cmp(a));
    n_inspections[0] * n_inspections[1]
}

pub fn p1() {
    let monkey_refs = make_monkeys();
    let monkey_business = compute_monkey_business(&monkey_refs, 20, |v| v / 3);
    println!("{}", monkey_business);
}

pub fn p2() {
    let monkey_refs = make_monkeys();
    let divisors: HashSet<u64> = monkey_refs.iter().map(|m| m.borrow().divisor).collect();
    let prod_divisors: u64 = divisors.iter().product();
    let monkey_business = compute_monkey_business(&monkey_refs, 10000, |v| v % prod_divisors);
    println!("{}", monkey_business);
}
