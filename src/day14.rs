use std::fs;
use std::collections::HashSet;
use std::collections::HashMap;

use self::NextPosition::*;

type Pos = (i32, i32);
type Polygon = Vec<Pos>;
type Polygons = Vec<Polygon>;
type Blocks = HashSet<Pos>;
type RowsPerCol = HashMap<i32, Vec<i32>>;

fn iter_segments(polygons: &Polygons) -> impl Iterator<Item=(Pos, Pos)> + '_ {
    polygons.iter()
        .map(|polygon| {
            let points = polygon.iter();
            let mut next_points = polygon.iter();
            next_points.next();
            points.zip(next_points)
                .map(|(a, b)| (*a, *b))
        })
        .flatten()
}

fn get_walls() -> Blocks {
    let polygons: Vec<Polygon> = fs::read_to_string("data/day14.txt")
        .expect("Should be able to read file")
        .split_terminator('\n')
        .map(|line| {
            line.split(" -> ")
                .map(|point| {
                    let (a, b) = point.split_once(',').unwrap();
                    (b.parse().unwrap(), a.parse().unwrap())
                })
                .collect()
        })
        .collect();

    let mut walls = HashSet::new();
    for (a, b) in iter_segments(&polygons) {
        if a.0 == b.0 {
            let cols = if a.1 <= b.1 {
                a.1..=b.1
            } else {
                b.1..=a.1
            };
            for c in cols {
                walls.insert((a.0, c));
            }
        } else {
            assert_eq!(a.1, b.1);
            let rows = if a.0 <= b.0 {
                a.0..=b.0
            } else {
                b.0..=a.0
            };
            for r in rows {
                walls.insert((r, a.1));
            }
        }
    }
    walls
}

fn make_wall_rows_per_col(walls: &Blocks) -> RowsPerCol {
    let mut rows_per_col = HashMap::new();
    for (r, c) in walls.iter() {
        rows_per_col.entry(*c).or_insert(Vec::new()).push(*r);
    }
    for rows in rows_per_col.values_mut() {
        rows.sort();
    }
    rows_per_col
}

enum NextPosition {
    Void,
    Blocked,
    Next(Pos),
}

fn next_position_below(current: &Pos, rows_per_col: &RowsPerCol) -> NextPosition {
    match rows_per_col.get(&current.1) {
        Some(rows) => {
            match rows.iter().filter(|r| *r > &current.0).next() {
                Some(r) => {if *r == current.0 + 1 {
                    Blocked
                } else {
                    Next((*r - 1, current.1))
                }},
                None => Void
            }
        },
        None => Void,
    }
}

fn is_position_free(current: &Pos, rows_per_col: &RowsPerCol) -> bool {
    match rows_per_col.get(&current.1) {
        Some(rows) => {rows.binary_search(&current.0).is_err()},
        None => {true},
    }
}

fn next_sand_position(current: &Pos, rows_per_col: &RowsPerCol) -> NextPosition {
    match next_position_below(current, rows_per_col) {
        Void => {return Void},
        Blocked => {},
        Next(p) => {return Next(p)},
    };
    let left = (current.0 + 1, current.1 - 1);
    if is_position_free(&left, rows_per_col) {
        return Next(left);
    }
    let right = (current.0 + 1, current.1 + 1);
    if is_position_free(&right, rows_per_col) {
        Next(right)
    } else {
        Blocked
    }
}

#[allow(dead_code)]
fn print_sand(sand: &Blocks, walls: &Blocks, min_row: i32, max_row: i32, min_col: i32, max_col: i32) {
    if sand.len() > 0 {
        for r in (min_row - 10)..(max_row + 10) {
            for c in (min_col - 10)..(max_col + 10) {
                if sand.contains(&(r, c)) {
                    print!("o");
                } else {
                    // if is_wall(&(r, c), &polygons) {
                    if walls.contains(&(r, c)) {
                        print!("#");
                    } else {
                        print!(".");
                    }
                }
            }
            println!();
        }
    }
    println!("{:?}", sand);
}

pub fn p1() {
    let walls = get_walls();
    // let min_row = walls.iter().map(|p| p.0).min().unwrap();
    // let min_col = walls.iter().map(|p| p.1).min().unwrap();
    // let max_row = walls.iter().map(|p| p.0).max().unwrap();
    // let max_col = walls.iter().map(|p| p.1).max().unwrap();
    let mut rows_per_col = make_wall_rows_per_col(&walls);

    let mut current = (0, 500);
    let mut sand = HashSet::new();
    loop {
        match next_sand_position(&current, &rows_per_col) {
            Void => {
                break;
            },
            Blocked => {
                sand.insert(current);
                let rows = rows_per_col.entry(current.1).or_insert(Vec::new());
                match rows.binary_search(&&current.0) {
                    Ok(_pos) => {},
                    Err(pos) => {rows.insert(pos, current.0)}
                }
                current = (0, 500);
            }
            Next(p) => {
                current = p;
            }
        }
        // print_sand(&sand, &walls, min_row, max_row, min_col, max_col);
    }
    println!("{}", sand.len());
}

pub fn p2() {
    let mut walls = get_walls();
    // let min_row = walls.iter().map(|p| p.0).min().unwrap();
    // let min_col = walls.iter().map(|p| p.1).min().unwrap();
    let max_row = walls.iter().map(|p| p.0).max().unwrap();
    // let max_col = walls.iter().map(|p| p.1).max().unwrap();
    for c in -500..1000 {
        walls.insert((max_row + 2, c));
    }
    let mut rows_per_col = make_wall_rows_per_col(&walls);

    let mut current = (0, 500);
    let mut sand = HashSet::new();
    loop {
        match next_sand_position(&current, &rows_per_col) {
            Void => {
                panic!("should not void");
            },
            Blocked => {
                sand.insert(current);
                if current == (0, 500) {
                    break;
                }
                let rows = rows_per_col.entry(current.1).or_insert(Vec::new());
                match rows.binary_search(&&current.0) {
                    Ok(_pos) => {},
                    Err(pos) => {rows.insert(pos, current.0)}
                }
                current = (0, 500);
            }
            Next(p) => {
                current = p;
            }
        }
        // print_sand(&sand, &walls, min_row, max_row, min_col, max_col);
    }
    println!("{}", sand.len());
}
