use std::{collections::{HashMap, HashSet, VecDeque}, fs};

const END: u32 = 26;

type Id = String;
type Nodes = HashMap<Id, Node>;
type DistanceMap = HashMap<Id, HashMap<Id, u32>>;

#[derive(Debug)]
struct Node {
    name: Id,
    flow: u32,
    neighbors: Vec<Id>,
}

impl From<&str> for Node {
    fn from(raw: &str) -> Self {
        let raw_fields = raw.replace("Valve ", "")
            .replace(" has flow rate=", ":")
            .replace("; tunnels lead to valves ", ":")
            .replace("; tunnel leads to valve ", ":");
        let fields: Vec<&str> = raw_fields
            .split(':')
            .collect();
        Self{
            name: fields[0].to_string(),
            flow: fields[1].parse().unwrap(),
            neighbors: fields[2].split(", ").map(|n| n.to_string()).collect(),
        }
    }
}

fn make_distances_from(nodes: &Nodes, source: &Id) -> HashMap<Id, u32> {
    let mut queue: VecDeque<Id> = VecDeque::new();
    queue.push_back(source.clone());
    let mut visited: HashSet<Id> = HashSet::new();
    visited.insert(source.clone());
    let mut distances: HashMap<Id, u32> = HashMap::new();
    distances.insert(source.clone(), 0);

    while !queue.is_empty() {
        let id = queue.pop_front().unwrap();
        for neighbor in nodes[&id].neighbors.iter() {
            if visited.contains(neighbor) {
                continue;
            }
            visited.insert(neighbor.clone());
            queue.push_back(neighbor.clone());
            let d = distances[&id] + 1;
            distances.insert(neighbor.clone(), d);
        }
    }

    distances
}

fn make_distance_map(start: &Id, nodes: &Nodes) -> DistanceMap {
    let mut distance_map = HashMap::new();
    let mut working_valves: Vec<Id> = nodes.values()
        .filter(|n| n.flow > 0)
        .map(|n| n.name.to_string())
        .collect();
    working_valves.push(start.clone());
    for source in working_valves.iter() {
        let distances = make_distances_from(nodes, source);
        for (target, distance) in distances {
            if source == &target {
                continue;
            }
            if nodes[&target].flow == 0 {
                continue;
            }
            distance_map.entry(source.clone()).or_insert(HashMap::new()).insert(target.clone(), distance);
        }
    }
    distance_map
}

#[derive(Clone)]
struct State {
    locations: HashMap<String, Location>,
    open: HashSet<Id>,
}

#[derive(Clone)]
struct Location {
    valve: Id,
    time: u32,
}

impl State {
    fn new(valve: Id) -> Self {
        let mut locations = HashMap::new();
        locations.insert("you".to_string(), Location{valve: valve.clone(), time: 1});
        locations.insert("elephant".to_string(), Location{valve: valve.clone(), time: 1});
        Self {
            locations,
            open: HashSet::new(),
        }
    }
}

fn get_max_pressure(state: &State, distance_map: &DistanceMap, nodes: &Nodes) -> u32 {
    let entity = ["you", "elephant"].iter().min_by_key(|e| state.locations[&e.to_string()].time).unwrap().to_string();
    distance_map[&state.locations[&entity].valve].iter()
        .filter_map(|(other, distance)| {
            if state.locations[&entity].time + distance + 1 >= END {
                return None;
            }
            if state.open.contains(other) {
                return None;
            }
            let mut new_state = state.clone();

            new_state.locations.get_mut(&entity).unwrap().time += distance + 1;
            new_state.locations.get_mut(&entity).unwrap().valve = other.clone();
            new_state.open.insert(other.clone());

            let this_valve_pressure = nodes[other].flow * (END - new_state.locations[&entity].time + 1);
            Some(this_valve_pressure + get_max_pressure(&new_state, distance_map, nodes))
        })
        .max().unwrap_or(0)
}

pub fn p2() {
    let nodes: Nodes = fs::read_to_string("data/day16.txt")
        .expect("Should be able to read file")
        .split_terminator('\n')
        .map(|line| {
            let node: Node = line.into();
            (node.name.to_string(), node)
        })
        .collect();

    let start = "AA".to_string();
    let distance_map = make_distance_map(&start, &nodes);
    // for (s, ds) in distance_map.iter() {
    //     for (t, d) in ds.iter() {
    //         println!("{} -- {}: {}", s, t, d);
    //     }
    // }

    let state = State::new(nodes[&start].name.to_string());
    println!("{}", get_max_pressure(&state, &distance_map, &nodes));
}
