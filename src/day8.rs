use std::fs;
use std::slice::Iter;
use std::collections::HashSet;

use itertools::Itertools;

use self::Direction::*;

enum Direction {
    West,
    East,
    North,
    South,
}

impl Direction {
    pub fn iterator() -> Iter<'static, Direction> {
        static DIRECTIONS: [Direction; 4] = [West, East, North, South];
        DIRECTIONS.iter()
    }

    pub fn to_unit_vector(&self) -> (i32, i32) {
        match self {
            West => {(0, -1)},
            East => {(0, 1)},
            North => {(-1, 0)},
            South => {(1, 0)},
        }
    }
}

fn tile_from_direction(n_rows: usize, n_cols: usize, direction: &Direction, i: usize, j: usize) -> (usize, usize) {
    let (row, col) = match direction {
        West => (i, j),
        East => (i, n_cols - j - 1),
        North => (j, i),
        South => (n_rows - j - 1, i),
    };
    (row, col)
}

fn get_tree_grid() -> Vec<Vec<i32>> {
    fs::read_to_string("data/day8.txt")
        .expect("Should be able to read the file")
        .split_whitespace()
        .map(|line| line.chars().map(|c| c.to_digit(10).unwrap() as i32).collect())
        .collect()
}

pub fn p1() {
    let tree_grid = get_tree_grid();

    // println!("{:?}", tree_grid);

    let n_rows = tree_grid.len();
    let n_cols = tree_grid[0].len();

    let mut visible_trees = HashSet::new();
    for i in 0..n_rows {
        for direction in Direction::iterator() {
            let mut highest = -1;
            for j in 0..n_cols {
                let (row, col) = tile_from_direction(n_rows, n_cols, &direction, i, j);
                let tree_height = tree_grid[row][col];
                if tree_height > highest {
                    visible_trees.insert((row, col));
                    highest = tree_height;
                }
            }
        }
    }

    // println!("{:?}", visible_trees);
    println!("{}", visible_trees.len());
}

fn is_in_grid(grid: &Vec<Vec<i32>>, tile: (i32, i32)) -> bool {
    tile.0 >= 0 && tile.1 >= 0 && tile.0 < grid.len() as i32 && tile.1 < grid[0].len() as i32
}

fn compute_scenic_score(tree_grid: &Vec<Vec<i32>>, row: usize, col: usize) -> u32 {
    let base_height = tree_grid[row][col];
    let mut score = 1;
    for direction in Direction::iterator() {
        let mut count = 0;
        let vector = direction.to_unit_vector();
        for distance in 1.. {
            let tile = (row as i32 + vector.0 * distance, col as i32 + vector.1 * distance);
            if !is_in_grid(tree_grid, tile) {
                break;
            }
            count += 1;
            let height = tree_grid[tile.0 as usize][tile.1 as usize];
            if height >= base_height {
                break;
            }
        }
        score *= count;
    }
    score
}

pub fn p2() {
    let tree_grid = get_tree_grid();
    // println!("{}", compute_scenic_score(&tree_grid, 1, 2));
    // println!("{}", compute_scenic_score(&tree_grid, 3, 2));

    let n_rows = tree_grid.len();
    let n_cols = tree_grid[0].len();

    let highest_scenic_score = (0..n_rows)
        .cartesian_product(0..n_cols)
        .map(|(i, j)| compute_scenic_score(&tree_grid, i, j))
        .max()
        .unwrap();
    println!("{}", highest_scenic_score);
}
