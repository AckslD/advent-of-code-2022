use std::fs;
use std::ops::{Add, Sub};
use std::collections::HashSet;

#[derive(Debug)]
enum Direction {
    R,
    L,
    U,
    D,
}

#[derive(Debug)]
struct Motion {
    direction: Direction,
    count: u32,
}

impl TryFrom<&str> for Motion {
    type Error = String;

    fn try_from(raw: &str) -> Result<Self, Self::Error> {
        let (direction_raw, count_raw) = raw.split_once(' ').ok_or("not a space")?;
        let count: u32 = count_raw.parse().or(Err("could not parse count"))?;
        match direction_raw {
            "R" => Ok(Motion{direction: Direction::R, count}),
            "L" => Ok(Motion{direction: Direction::L, count}),
            "U" => Ok(Motion{direction: Direction::U, count}),
            "D" => Ok(Motion{direction: Direction::D, count}),
            _ => Err(format!("unknown direction {}", direction_raw)),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
struct Pos {
    x: i32,
    y: i32,
}

impl Pos {
    fn zero() -> Self {
        Pos{x: 0, y: 0}
    }

    fn map(&self, f: impl Fn(i32) -> i32) -> Self {
        Pos{x: f(self.x), y: f(self.y)}
    }

    // fn greater_than_one(&self) -> Self {
    //     self.map(|n| (n > 1) as i32)
    // }

    fn normalize(&self) -> Self {
        self.map(|n| if n == 0 {0} else {n / n.abs()})
    }
}

impl Add for &Pos {
    type Output = Pos;
    fn add(self, other: &Pos) -> Pos {
        return Pos{x: self.x + other.x, y: self.y + other.y}
    }
}

impl Sub for &Pos {
    type Output = Pos;
    fn sub(self, other: &Pos) -> Pos {
        return Pos{x: self.x - other.x, y: self.y - other.y}
    }
}

#[derive(Debug)]
struct Rope {
    knots: Vec<Pos>,
}

impl Rope {
    fn init(n: usize) -> Self {
        let mut knots = Vec::new();
        for _ in 0..n {
            knots.push(Pos::zero());
        }
        Rope{knots}
    }

    fn update_previous(&mut self, knot_idx: usize) {
        let knot = self.knots[knot_idx];
        let next_knot = self.knots[knot_idx + 1];

        let difference = &next_knot - &knot;
        if difference.x.abs() >= 2 || difference.y.abs() >= 2 {
            self.knots[knot_idx] = &knot + &difference.normalize();
        }

        if knot_idx > 0 {
            self.update_previous(knot_idx - 1);
        }
    }

    fn move_head(&mut self, direction: &Direction) {
        use Direction::*;
        let knot_idx = self.knots.len() - 1;
        let head = &mut self.knots[knot_idx];
        match direction {
            R => {head.x += 1}
            L => {head.x -= 1}
            U => {head.y += 1}
            D => {head.y -= 1}
        }
        self.update_previous(knot_idx - 1);
    }
}

fn read_motions(filepath: &str) -> Vec<Motion> {
    fs::read_to_string(filepath)
        .expect("Should be able to read the file")
        .split_terminator('\n')
        .map(|line| line.try_into().unwrap())
        .collect()
}

fn find_visited_tail_positions(motions: &Vec<Motion>, rope: &mut Rope) -> HashSet<Pos> {
    let mut tail_visited = HashSet::new();
    for motion in motions {
        // println!("{:?}", motion);
        for _ in 0..motion.count {
            rope.move_head(&motion.direction);
            // println!("{:?}", rope);
            tail_visited.insert(rope.knots[0]);
        }
    }
    tail_visited
}

pub fn p1() {
    let motions = read_motions("data/day9.txt");
    let mut rope = Rope::init(2);
    let tail_visited = find_visited_tail_positions(&motions, &mut rope);
    println!("{}", tail_visited.len());
}

pub fn p2() {
    let motions = read_motions("data/day9.txt");
    let mut rope = Rope::init(10);
    let tail_visited = find_visited_tail_positions(&motions, &mut rope);
    println!("{}", tail_visited.len());
}
