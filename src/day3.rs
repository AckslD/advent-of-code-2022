use std::fs;
use std::collections::HashSet;

fn get_lines() -> Vec<String> {
    fs::read_to_string("data/day3.txt")
        .expect("Should be able to read file")
        .split_whitespace()
        .map(|line| line.to_string())
        .collect()
}

fn to_priorities(letters: &str) -> HashSet<u32> {
    letters.chars().map(|c| {
        match c {
            'a'..='z' => c as u32 - ('a' as u32) + 1,
            'A'..='Z' => c as u32 - ('A' as u32) + 27,
            _ => panic!("unexpected letter '{}'", c),
        }
    }).collect()
}

pub fn p1() {
    let total: u32 = get_lines().iter()
        .map(|line| line.split_at(line.len() / 2))
        .map(|(left, right)| {
            to_priorities(left).intersection(&to_priorities(right))
                .map(|p| *p)
                .collect()
        })
        .map(|duplicates: Vec<u32>| duplicates[0])
        .sum();
    println!("{}", total);
}

pub fn p2() {
    let lines: Vec<String> = get_lines();
    let total: u32 = (0..lines.len()).step_by(3)
        .map(|i| {
            lines[i..(i+3)].iter()
                .map(|line| to_priorities(line))
                .reduce(|a, b| {
                    a.intersection(&b)
                        .map(|p| *p).collect()
                })
                .unwrap()
        })
        .map(|duplicates: HashSet<u32>| duplicates.iter().next().cloned().unwrap())
        .sum();
    println!("{}", total);
}
