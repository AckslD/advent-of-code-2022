use std::{collections::HashSet, fs};
use std::ops::{Add, Sub};

use self::Direction::*;

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
struct Pos {
    x: i32,
    y: i32,
}

impl Pos {
    fn shift_x(&self, amount: i32) -> Self {
        Self{x: self.x + amount, y: self.y}
    }

    fn shift_y(&self, amount: i32) -> Self {
        Self{x: self.x, y: self.y + amount}
    }
}

impl Add for &Pos {
    type Output = Pos;
    fn add(self, other: &Pos) -> Pos {
        return Pos{x: self.x + other.x, y: self.y + other.y}
    }
}

impl Sub for &Pos {
    type Output = Pos;
    fn sub(self, other: &Pos) -> Pos {
        return Pos{x: self.x - other.x, y: self.y - other.y}
    }
}

type Shape = HashSet<Pos>;

fn make_shape(raw: &str) -> Shape {
    let mut shape: Shape = HashSet::new();
    let lines: Vec<String> = raw.split_terminator('\n').map(|l| l.to_string()).collect();
    let n = lines.len();
    for (i, line) in lines.iter().enumerate() {
        for (j, c) in line.chars().enumerate() {
            if c == '#' {
                shape.insert(Pos{x: j as i32, y: (n - i - 1) as i32});
            }
        }
    }
    shape
}

fn load_shapes() -> Vec<Shape> {
    fs::read_to_string("data/day17_shapes.txt")
        .unwrap()
        .split_terminator("\n\n")
        .map(|shape| make_shape(shape))
        .collect()
}

#[derive(Clone, Copy)]
enum Direction {
    Left,
    Right,
}

struct Wind {
    directions: Vec<Direction>,
    idx: usize,
}

impl Iterator for Wind {
    type Item = Direction;

    fn next(&mut self) -> Option<Self::Item> {
        let direction = self.directions[self.idx];
        self.idx += 1;
        self.idx %= self.directions.len();
        Some(direction)
    }
}

fn load_wind() -> Wind {
    let directions: Vec<Direction> = fs::read_to_string("data/day17.txt")
        .unwrap()
        .trim()
        .chars()
        .map(|c| match c {
            '<' => Left,
            '>' => Right,
            _ => panic!("unknown wind `{}`", c),
        })
        .collect();
    Wind{directions, idx: 0}
}

struct Rock {
    shape_idx: usize,
    pos: Pos,
}

struct Tower {
    tiles: HashSet<Pos>,
    height: u32,
    shapes: Vec<Shape>,
}

impl Tower {
    fn new(shapes: Vec<Shape>) -> Self {
        Tower{tiles: HashSet::new(), height: 0, shapes}
    }

    fn is_free(&self, pos: &Pos) -> bool {
        if self.tiles.contains(pos) {
            return false;
        }
        if pos.x < 0 || pos.x > 6 || pos.y < 0 {
            return false;
        }
        true
    }
}

fn get_absolute_tiles(tower: &Tower, rock: &Rock, pos: &Pos) -> Vec<Pos> {
    tower.shapes[rock.shape_idx].iter()
        .map(|r| r + pos)
        .collect()
}

fn valid_pos(tower: &Tower, rock: &Rock, pos: &Pos) -> bool {
    for absolute_pos in get_absolute_tiles(tower, rock, pos) {
        if !tower.is_free(&absolute_pos) {
            return false;
        }
    }
    true
}

fn try_shift_wind(tower: &Tower, wind: &mut Wind, rock: &mut Rock) {
    let new_pos = match wind.next().unwrap() {
        Left => {rock.pos.shift_x(-1)},
        Right => {rock.pos.shift_x(1)},
    };
    if valid_pos(tower, rock, &new_pos) {
        rock.pos = new_pos;
    }
}

fn try_fall(tower: &Tower, rock: &mut Rock) -> bool {
    let new_pos = rock.pos.shift_y(-1);
    if valid_pos(tower, rock, &new_pos) {
        rock.pos = new_pos;
        true
    } else {
        false
    }
}

fn simulate_rock(tower: &Tower, wind: &mut Wind, rock: &mut Rock) {
    loop {
        try_shift_wind(tower, wind, rock);
        let moved = try_fall(tower, rock);
        if !moved {
            break;
        }
    }
}

fn absorb_rock(tower: &mut Tower, rock: &Rock) {
    for pos in get_absolute_tiles(tower, rock, &rock.pos) {
        if pos.y >= tower.height as i32 {
            tower.height = (pos.y + 1) as u32;
        }
        tower.tiles.insert(pos);
    }
}

fn does_repeat(heights: &Vec<u32>, size: usize, offset: usize) -> bool {
    // TODO
    // let first: Vec<u32> = heights.iter().skip(offset).copied().collect();
    // let second: Vec<u32> = heights.iter().skip(offset).copied().collect();
    // if first.len() != second.len() {
    //     return false;
    // }
    let mut at_least_one = false;
    for (h1, h2) in heights.iter()
        .skip(offset)
        .take(size)
        .map(|h| h - heights[offset])
        .zip(
            heights.iter()
                .skip(size + offset)
                .map(|h| h - heights[size + offset])
        ) {
        at_least_one = true;
        if h1 != h2 {
            return false;
        }
    }
    at_least_one
}

#[allow(dead_code)]
fn print_tower(tower: &Tower) {
    // for r in 0..tower.height {
    for r in 0..10 {
        for c in 0..7 {
            if tower.tiles.contains(&Pos{x: c, y: tower.height as i32 - r as i32 - 1}) {
                print!("#");
            } else {
                print!(".");
            }
        }
        println!();
    }
    println!("...");
}

fn make_heights(num: usize) -> Vec<u32> {
    let shapes = load_shapes();
    let num_shapes = shapes.len();
    let mut wind = load_wind();

    let mut tower = Tower::new(shapes);
    let mut shape_idx = 0;
    let mut heights = Vec::new();
    for _ in 0..num {
        let mut rock = Rock{shape_idx, pos: Pos{x: 2, y: tower.height as i32 + 3}};

        simulate_rock(&tower, &mut wind, &mut rock);
        absorb_rock(&mut tower, &rock);
        heights.push(tower.height);

        shape_idx += 1;
        shape_idx %= num_shapes;

        // print_tower(tower);
    }
    heights
}

pub fn p1() {
    let heights = make_heights(2022);

    println!("{}", heights[heights.len() - 1]);
}

pub fn p2() {
    let num = 20220;
    let heights = make_heights(num);

    let mut pattern = None;
    for size in 5..(num / 2) {
        for offset in 0..size {
            if offset + size > num {
                break;
            }
            if does_repeat(&heights, size, offset) {
                println!("repeats after {} with offset {}", size, offset);
                pattern = Some((size, offset));
                break;
            }
        }
        if pattern.is_some() {
            break;
        }
    }

    let round: u64 = 1000000000000 - 1;
    let (size, offset) = pattern.unwrap();
    let idx = (round - offset as u64) % (size as u64);
    let cycles = (round - offset as u64) / (size as u64);
    let per_cycle = heights[size + offset] - heights[offset];
    let extra = heights[idx as usize + offset];// - heights[offset];
    // println!("idx={}, cycles={}, per_cycle={}, extra={}", idx, cycles, per_cycle, extra);
    println!("{}", per_cycle as u64 * cycles + extra as u64);
}
