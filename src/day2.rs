use std::fs;
use std::str::FromStr;
use std::cmp::Ordering;
use std::ops::Add;

#[derive(Debug)]
struct ParseHandError;

#[derive(Debug, Clone, Copy)]
enum Outcome {
    Lose = 0,
    Draw = 3,
    Win = 6,
}

impl From<&Hand> for Outcome {
    fn from(hand: &Hand) -> Self {
        use Hand::*;
        use Outcome::*;
        match hand {
            Rock => Lose,
            Paper => Draw,
            Scissor => Win,
        }
    }
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Clone, Copy)]
enum Hand {
    Rock = 1,
    Paper = 2,
    Scissor = 3,
}

impl From<u8> for Hand {
    fn from(i: u8) -> Self {
        use Hand::*;
        match i {
            1 => Rock,
            2 => Paper,
            3 => Scissor,
            _ => panic!("cannot convert {} to Hand", i)
        }
    }
}

impl Add<u8> for &Hand {
    type Output = Hand;

    fn add(self, rhs: u8) -> Self::Output {
        Hand::from((*self as u8 + rhs - 1) % 3 + 1)
    }
}

impl Hand {
    fn such_that(&self, outcome: &Outcome) -> Hand {
        use Outcome::*;
        match outcome {
            Lose => self + 2,
            Draw => *self,
            Win => self + 1,
        }
    }
}

impl FromStr for Hand {
    type Err = ParseHandError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use Hand::*;
        match s {
            "A" => Ok(Rock),
            "B" => Ok(Paper),
            "C" => Ok(Scissor),
            "X" => Ok(Rock),
            "Y" => Ok(Paper),
            "Z" => Ok(Scissor),
            _ => Err(ParseHandError),
        }
    }
}

impl Ord for Hand {
    fn cmp(&self, other: &Self) -> Ordering {
        use Hand::*;
        use Ordering::*;
        match (self, other) {
            (Rock, Scissor) => Greater,
            (Scissor, Rock) => Less,
            _ => (*self as u8).cmp(&(*other as u8))
        }
    }
}

fn score_p1(theirs: &Hand, yours: &Hand) -> u32 {
    use Ordering::*;
    (*yours as u32) + match yours.cmp(theirs) {
        Less => 0,
        Equal => 3,
        Greater => 6,
    }
}

fn score_p2(theirs: &Hand, yours: &Hand) -> u32 {
    let outcome: Outcome = yours.into();
    (outcome as u32) + (theirs.such_that(&outcome) as u32)
}

fn get_rounds() -> Vec<Vec<Hand>> {
    fs::read_to_string("data/day2.txt")
        .expect("Should be able to read file")
        .split_terminator("\n")
        .map(|line| {
            line.split(" ")
                .map(|c| c.parse::<Hand>().expect("Should be a valid hand"))
                .collect()
        })
        .collect()
}

pub fn p1() {
    let total: u32 = get_rounds().into_iter()
        .map(|round| score_p1(&round[0], &round[1]))
        .sum();
    println!("{}", total);
}

pub fn p2() {
    let total: u32 = get_rounds().into_iter()
        .map(|round| score_p2(&round[0], &round[1]))
        .sum();
    println!("{}", total);
}
