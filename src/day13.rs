use std::fs;
use std::cmp::Ordering;
use std::collections::HashMap;

use self::Packet::*;

#[derive(Debug, PartialEq, Eq)]
enum Packet {
    Int(u32),
    List(Vec<Packet>),
}

impl From<&str> for Packet {
    fn from(raw: &str) -> Self {
        let mut parents: Vec<Vec<Packet>> = Vec::new();
        let mut current: Vec<Packet> = Vec::new();
        let mut raw_num: String = String::new();
        for c in raw.chars() {
            match c {
                '[' => { // step down
                    parents.push(current);
                    current = Vec::new();
                },
                ']' => { // step up
                    if raw_num.len() > 0 {
                        current.push(Int(raw_num.parse().unwrap()));
                        raw_num.clear();
                    }
                    let mut parent = parents.pop().unwrap();
                    parent.push(List(current));
                    current = parent;
                },
                ',' => {  // commit current
                    if raw_num.len() > 0 {
                        current.push(Int(raw_num.parse().unwrap()));
                        raw_num.clear();
                    }
                },
                _ => {  // add to current
                    raw_num.push(c);
                },
            }
        }
        List(current)
    }
}

impl PartialOrd for Packet {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        // println!("cmp");
        use Ordering::*;
        match (self, other) {
            (Int(s), Int(o)) => s.partial_cmp(o),
            (List(ss), List(os)) => {
                let mut ss_iter = ss.iter();
                let mut os_iter = os.iter();
                loop {
                    let s = ss_iter.next();
                    let o = os_iter.next();
                    match (s, o) {
                        (Some(s), Some(o)) => {
                            let ord = s.partial_cmp(o);
                            match ord {
                                Some(Equal) => {},
                                _ => {return ord;},
                            }
                        },
                        (Some(_), None) => {return Some(Greater);},
                        (None, Some(_)) => {return Some(Less);},
                        (None, None) => {return Some(Equal);},
                    }
                }
            },
            (Int(s), List(_)) => List(vec![Int(*s)]).partial_cmp(other),
            (List(_), Int(o)) => self.partial_cmp(&List(vec![Int(*o)])),
        }
    }
}

fn get_pairs() -> Vec<Vec<Packet>> {
    fs::read_to_string("data/day13.txt")
        .expect("Should be able to read file")
        .split_terminator("\n\n")
        .map(|pair| {
            pair.split_terminator('\n')
                .map(|s| s.into())
                .collect()
        })
        .collect()
}

pub fn p1() {
    let pairs = get_pairs();

    let total: usize = pairs.iter().enumerate()
        .filter(|(_, pair)| pair[0] <= pair[1])
        .map(|(i, _)| i + 1)
        .sum();
    println!("{:?}", total);
}

pub fn p2() {
    let pairs = get_pairs();

    let mut packets: Vec<Packet> = pairs.into_iter().flatten().collect();
    packets.sort_by(|a, b| a.partial_cmp(b).unwrap());
    let divider_packets = vec![
        List(vec![List(vec![Int(2)])]),
        List(vec![List(vec![Int(6)])]),
    ];
    let mut divider_indices: HashMap<usize, usize> = HashMap::new();
    for (i, packet) in packets.iter().enumerate() {
        for (j, divider_packet) in divider_packets.iter().enumerate() {
            if divider_indices.contains_key(&j) {
                continue;
            }
            if divider_packet <= packet {
                divider_indices.insert(j, i + j + 1);
            }
        }
    }
    println!("{:?}", divider_indices[&0] * divider_indices[&1]);
}
