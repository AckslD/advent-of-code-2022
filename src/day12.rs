use std::fs;
use std::fmt;
use std::collections::VecDeque;
use std::io::{stdout, Write, Stdout};
use crossterm::{cursor, ExecutableCommand};
use std::collections::HashSet;
use std::collections::HashMap;

use self::Tile::*;

#[derive(Debug, PartialEq, Clone, Copy)]
enum Tile {
    Start,
    End,
    Height(u8),
}

impl From<char> for Tile {
    fn from(c: char) -> Self {
        match c {
            'S' => Tile::Start,
            'E' => Tile::End,
            'a'..='z' => Tile::Height(c as u8 - b'a'),
            _ => panic!("unknown tile {}", c),
        }
    }
}

impl fmt::Display for Tile {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", match self {
            Start => 'S',
            End => 'E',
            Height(c) => (*c + b'a') as char,
        })
    }
}

type Pos = (i32, i32);
type Elevation = Vec<Vec<Tile>>;

fn get_tile(pos: &Pos, elevation: &Elevation) -> Tile {
    elevation[pos.0 as usize][pos.1 as usize]
}

fn get_height(pos: &Pos, elevation: &Elevation) -> u8 {
    match get_tile(pos, elevation) {
        Start => 0,
        End => b'z' - b'a',
        Height(h) => h,
    }
}

fn neighbors(pos: &Pos, elevation: &Elevation) -> Vec<Pos> {
    let n_rows = elevation.len() as i32;
    let n_cols = elevation[0].len() as i32;
    let current_height = get_height(pos, elevation);

    [(pos.0 + 1, pos.1), (pos.0 - 1, pos.1), (pos.0, pos.1 + 1), (pos.0, pos.1 - 1)].iter()
        .filter(|n| n.0 >= 0 && n.0 < n_rows && n.1 >= 0 && n.1 < n_cols)
        .filter(|n| get_height(n, elevation) <= current_height + 1)
        .copied()
        .collect()
}

#[allow(dead_code)]
fn print_grid(elevation: &Elevation) {
    for row in elevation {
        for tile in row {
            print!("{}", tile);
        }
        println!();
    }
}

#[allow(dead_code)]
fn update_stdout_grid(stdout: &mut Stdout, elevation: &Elevation, pos: &Pos) {
    write!(stdout, "{:?}", pos).unwrap();
    stdout.execute(cursor::RestorePosition).unwrap();
    stdout.execute(cursor::MoveUp((elevation.len() - pos.0 as usize) as u16)).unwrap();
    if pos.1 > 0 {
        stdout.execute(cursor::MoveRight(pos.1 as u16)).unwrap();
    }
    write!(stdout, "X").unwrap();
    stdout.execute(cursor::MoveLeft(1)).unwrap();
    std::thread::sleep(std::time::Duration::from_millis(1));
    write!(stdout, "{}", elevation[pos.0 as usize][pos.1 as usize]).unwrap();
    stdout.execute(cursor::RestorePosition).unwrap();
}

fn bfs(start: &Pos, elevation: &Elevation) -> (HashMap<Pos, Pos>, Option<Pos>) {
    let mut previous: HashMap<Pos, Pos> = HashMap::new();
    let mut queue: VecDeque<Pos> = VecDeque::new();
    queue.push_back(*start);
    let mut visited: HashSet<Pos> = HashSet::new();
    visited.insert(*start);
    // print_grid(elevation);
    let mut stdout = stdout();
    stdout.execute(cursor::SavePosition).unwrap();
    while !queue.is_empty() {
        let pos = queue.pop_front().unwrap();
        // update_stdout_grid(&mut stdout, elevation, &pos);
        if get_tile(&pos, elevation) == End {
            return (previous, Some(pos));
        }
        for neighbor in neighbors(&pos, elevation) {
            if visited.contains(&neighbor) {
                continue;
            }
            visited.insert(neighbor);
            previous.insert(neighbor, pos);
            queue.push_back(neighbor);
        }
    }
    (previous, None)
}

fn shortest_path(start: &Pos, elevation: &Elevation) -> Option<Vec<Pos>> {
    let (previous, end_pos) = bfs(start, elevation);
    let mut pos = end_pos?;
    let mut path: Vec<Pos> = Vec::new();
    path.push(pos);
    loop {
        pos = previous[&pos];
        path.push(pos);
        if pos == *start {
            break;
        }
    }
    path.reverse();
    Some(path)
}

fn find_start(elevation: &Elevation) -> Option<Pos> {
    for (r, row) in elevation.iter().enumerate() {
        for (c, tile) in row.iter().enumerate() {
            if tile == &Start {
                return Some((r as i32, c as i32));
            }
        }
    }
    None
}

fn read_elevation() -> Elevation {
    fs::read_to_string("data/day12.txt")
        .expect("Should be able to read file")
        .split_whitespace()
        .map(|line| line.chars().map(|c| c.into()).collect())
        .collect()
}

pub fn p1() {
    let elevation = read_elevation();

    let start = find_start(&elevation).unwrap();
    println!("{}", shortest_path(&start, &elevation).unwrap().len() - 1);
}

pub fn p2() {
    let elevation = read_elevation();

    let mut paths = Vec::new();
    for (r, row) in elevation.iter().enumerate() {
        for (c, tile) in row.iter().enumerate() {
            if !match tile {
                Start => true,
                Height(0) => true,
                _ => false,
            } {
                continue;
            }
            let pos = (r as i32, c as i32);
            match shortest_path(&pos, &elevation) {
                Some(path) => {paths.push(path.len() - 1)},
                None => {},
            };
        }
    }
    println!("{}", paths.iter().min().unwrap());
}
