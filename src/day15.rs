use std::fs;
use std::collections::HashMap;
use std::collections::HashSet;

#[derive(Debug, Hash, PartialEq, Eq, Clone, Copy)]
struct Pos {
    x: i64,
    y: i64,
}

impl From<&str> for Pos {
    fn from(raw: &str) -> Self {
        let (x, y) = raw.split_once(", ").unwrap();
        Pos{x: x.parse().unwrap(), y: y.parse().unwrap()}
    }
}

impl Pos {
    fn manhattan_distance(&self, other: &Pos) -> i64 {
        (self.x - other.x).abs() + (self.y - other.y).abs()
    }
}

fn get_beacon_per_sensor() -> HashMap<Pos, Pos> {
    fs::read_to_string(FILE)
        .expect("Should be able to read file")
        .split_terminator('\n')
        .map(|line| {
            let raw = line.replace("Sensor at ", "")
                .replace(": closest beacon is at ", ":")
                .replace("x=", "")
                .replace("y=", "");
            let (sensor, beacon) = raw.split_once(':').unwrap();
            (sensor.into(), beacon.into())
        })
        .collect()
}

fn make_non_positions(beacon_per_sensor: HashMap<Pos, Pos>) -> HashSet<i64> {
    let mut non_positions: HashSet<i64> = HashSet::new();
    for (sensor, beacon) in beacon_per_sensor.iter() {
        // println!("{:?} -> {:?}", sensor, beacon);
        let d = sensor.manhattan_distance(beacon);
        for x in (sensor.x - d)..=(sensor.x + d) {
            let pos = Pos{x, y: Y};
            if sensor.manhattan_distance(&pos) <= d {
                non_positions.insert(pos.x);
            }
        }
    }

    for (sensor, beacon) in beacon_per_sensor.iter() {
        for pos in [sensor, beacon].iter() {
            if pos.y == Y {
                // println!("removing {:?}", pos);
                non_positions.remove(&pos.x);
            }
        }
    }

    non_positions
}

// const Y: i64 = 10;
// const MAX: i64 = 20;
// const FILE: &'static str = "data/day15_0.txt";
const Y: i64 = 2_000_000;
const MAX: i64 = 4_000_000;
const FILE: &'static str = "data/day15.txt";

pub fn p1() {
    let beacon_per_sensor = get_beacon_per_sensor();
    let non_positions = make_non_positions(beacon_per_sensor);
    println!("{}", non_positions.len());
}

pub fn p2() {
    let beacon_per_sensor = get_beacon_per_sensor();
    // let n = beacon_per_sensor.len();

    let mut possible_positions: HashSet<Pos> = HashSet::new();
    for (_i, (sensor, beacon)) in beacon_per_sensor.iter().enumerate() {
        // println!("{}/{}", i, n);
        let d = sensor.manhattan_distance(beacon);
        for i in 0..d {
            for pos in [
                Pos{x: sensor.x + i, y: sensor.y - (d + 1) + i}, // NE
                Pos{x: sensor.x + (d + 1) - i, y: sensor.y + i}, // SE
                Pos{x: sensor.x - i, y: sensor.y + (d + 1) - i}, // SW
                Pos{x: sensor.x - (d + 1) + i, y: sensor.y - i}, // NW
            ] {
                if pos.x >= 0 && pos.x <= MAX && pos.y >= 0 && pos.y <= MAX {
                    possible_positions.insert(pos);
                }
            }
        }
    }

    let mut non_possible_positions: HashSet<Pos> = HashSet::new();
    for (_i, (sensor, beacon)) in beacon_per_sensor.iter().enumerate() {
        // println!("{}/{}", i, n);
        let d = sensor.manhattan_distance(beacon);
        for pos in possible_positions.iter() {
            if sensor.manhattan_distance(&pos) <= d {
                non_possible_positions.insert(*pos);
            }
        }
    }

    let positions: HashSet<Pos> = possible_positions.difference(&non_possible_positions).copied().collect();
    assert_eq!(positions.len(), 1);
    let pos = positions.iter().next().unwrap();
    // println!("{:?}", pos);
    println!("{}", pos.x * 4_000_000 + pos.y);
}
