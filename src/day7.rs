use std::fs;
use std::collections::HashMap;
use std::collections::HashSet;

#[derive(Debug)]
enum LogEntry {
    Root,
    Up,
    Cd(String),
    Ls,
    Dir(String),
    File((String, usize)),
}

fn parse_log_line(line: &str) -> LogEntry {
    use LogEntry::*;

    let words: Vec<&str> = line.split_whitespace().collect();
    match words[0] {
        "$" => {
            match words[1] {
                "cd" => {
                    match words[2] {
                        "/" => Root,
                        ".." => Up,
                        _ => Cd(words[2].to_string()),
                    }
                }
                "ls" => Ls,
                _ => panic!("could not parse {}", line),
            }
        }
        "dir" => Dir(words[1].to_string()),
        _ => File((words[1].to_string(), words[0].parse().unwrap()))
    }
}

// fn make_path(fragments: &Vec<String>) -> String {
//     fragments.join("/")
// }

fn find_sizes(log_entries: &Vec<LogEntry>) -> HashMap<String, usize> {
    use LogEntry::*;

    let mut current_parents = Vec::new();
    let mut sizes = HashMap::new();
    let mut checked = HashSet::new();
    let mut check = true;
    for log_entry in log_entries {
        match log_entry {
            Root => {current_parents.clear();},
            Up => {current_parents.pop();},
            Cd(name) => {current_parents.push(name.to_string());},
            Ls => {check = checked.insert(current_parents.join("/"));}
            Dir(_name) => {}
            File((_name, size)) => {
                if check {
                    for i in 0..(current_parents.len() + 1) {
                        *sizes.entry(current_parents[0..i].join("/")).or_insert(0) += size;
                    }
                }
            }
        }
    }
    sizes
}

fn get_sizes() -> HashMap<String, usize> {
    let log_entries: Vec<LogEntry> = fs::read_to_string("data/day7.txt")
        .expect("Should be able to read the file")
        .split_terminator('\n')
        .map(parse_log_line)
        .collect();

    find_sizes(&log_entries)
}

pub fn p1() {
    let sizes = get_sizes();

    let total: usize = sizes.iter()
        .map(|(_path, size)| *size)
        .filter(|size| *size <= 100_000)
        .sum();
    println!("{}", total);

}

pub fn p2() {
    let sizes = get_sizes();
    let available = 70_000_000 - sizes.get("").unwrap();
    let needed = 30_000_000 - available;
    // println!("{}", available);
    // println!("{}", needed);

    let mut sufficient_sizes: Vec<usize> = sizes.iter()
        .map(|(_path, size)| *size)
        .filter(|size| *size >= needed)
        .collect();
    // println!("{:?}", sufficient_sizes);
    sufficient_sizes.sort();
    println!("{}", sufficient_sizes[0]);
}
