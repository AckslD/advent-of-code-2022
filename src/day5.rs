use std::fs;

fn parse_stacks(raw: &str) -> Vec<Vec<char>> {
    let mut lines: Vec<&str> = raw.split_terminator('\n').collect();
    lines.pop();
    lines.reverse();
    let n = lines[0].len() / 4 + 1;
    let mut stacks = Vec::new();
    for _ in 0..n {
        stacks.push(Vec::new());
    }
    for line in lines.iter() {
        for (i, char) in line.chars().enumerate() {
            if i % 4 == 1 && char != ' ' {
                stacks[i / 4].push(char);
            }
        }
    }
    stacks
}

#[derive(Debug)]
struct Step {
    num: u32,
    from: usize,
    to: usize,
}

impl From<Vec<String>> for Step {
    fn from(v: Vec<String>) -> Self {
        Step{
            num: v[0].parse().unwrap(),
            from: v[1].parse().unwrap(),
            to: v[2].parse().unwrap(),
        }
    }
}

fn parse_procedure(raw: &str) -> Vec<Step> {
    let raw = raw.replace("move ", "");
    raw.split_terminator('\n')
        .map(|line| {
            line.split(" from ")
                .flat_map(|s| {
                    s.split(" to ")
                        .map(|s| s.to_string())
                })
                .collect::<Vec<String>>()
                .into()
        })
        .collect()
}

fn pop_crates(stack: &mut Vec<char>, num: u32, reverse: bool) -> Vec<char> {
    let mut popped = Vec::new();
    for _ in 0..num {
        popped.push(stack.pop().unwrap());
    }
    if reverse {
        popped.reverse()
    }
    popped
}

fn do_step(stacks: &mut Vec<Vec<char>>, step: &Step, reverse_pop: bool) {
    for c in pop_crates(&mut stacks[step.from - 1], step.num, reverse_pop) {
        stacks[step.to - 1].push(c)
    }
}

fn print_final_stack_tops(reverse_pop: bool) {
    let raw = fs::read_to_string("data/day5.txt")
        .expect("Should be able to read file");
    let (stacks_raw, procedure_raw) = raw.split_once("\n\n").unwrap().clone();
    let mut stacks = parse_stacks(stacks_raw);
    let procedure = parse_procedure(procedure_raw);
    for step in procedure {
        do_step(&mut stacks, &step, reverse_pop);
    }
    for stack in &mut stacks {
        print!("{}", stack.pop().unwrap());
    }
    println!("");
}

pub fn p1() {
    print_final_stack_tops(false);
}

pub fn p2() {
    print_final_stack_tops(true);
}
