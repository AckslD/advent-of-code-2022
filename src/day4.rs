use std::fs;

type Section = (u32, u32);

fn parse_section(raw: &str) -> Section {
    let (start, end) = raw.split_once('-').unwrap();
    (start.parse().unwrap(), end.parse().unwrap())
}

fn contains(a: Section, b: Section) -> bool {
    a.0 <= b.0 && a.1 >= b.1
}

fn overlap(a: Section, b: Section) -> bool {
    (a.0 <= b.0 && b.0 <= a.1) || (a.0 <= b.1 && b.1 <= a.1)
}

fn count_hits(check: &dyn Fn(Section, Section) -> bool) {
    let n_contains: u32 = fs::read_to_string("data/day4.txt")
        .expect("Should be able to read file")
        .split_whitespace()
        .map(|line| {
            let (first_raw, second_raw) = line.split_once(',').unwrap();
            let first = parse_section(first_raw);
            let second = parse_section(second_raw);
            (check(first, second) || contains(second, first)) as u32
        })
        .sum();
    println!("{}", n_contains);
}

pub fn p1() {
    count_hits(&contains);
}

pub fn p2() {
    count_hits(&overlap);
}
