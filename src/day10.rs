use std::fs;
use std::fmt;

use self::Operation::*;

#[derive(Debug, Clone, Copy)]
enum Operation {
    Noop,
    AddX(i32),
}

impl TryFrom<&str> for Operation {
    type Error = String;

    fn try_from(raw: &str) -> Result<Self, Self::Error> {
        let words: Vec<String> = raw.split_whitespace().map(|w| w.to_string()).collect();
        match words[0].as_str() {
            "noop" => Ok(Noop),
            "addx" => Ok(AddX(words[1].parse().or(Err("could not parse add value"))?)),
            _ => Err(format!("unknown operation {}", words[0])),
        }
    }
}

#[derive(Debug)]
struct OpCycles {
    op: Operation,
    remaining: u32,
}

struct CPU {
    x: i32,
    ops: Vec<Operation>,
    current_op: Option<OpCycles>,
    next_op: usize,
    cycle: u32,
}

impl CPU {
    fn new() -> Self {
        CPU{
            x: 1,
            ops: Vec::new(),
            current_op: None,
            next_op: 0,
            cycle: 1,
        }
    }

    fn supply_operations(&mut self, operations: &mut Vec<Operation>) {
        self.ops.append(operations);
    }

    fn tick(&mut self) -> Result<(), ()> {
        self.cycle += 1;
        if self.current_op.is_none() {
            self.start_next_op()?;
        }
        let mut current_op = self.current_op.as_mut().unwrap();
        current_op.remaining -= 1;
        if current_op.remaining == 0 {
            self.do_op();
            self.start_next_op()?;
        }
        Ok(())
    }

    fn start_next_op(&mut self) -> Result<(), ()> {
        if self.next_op >= self.ops.len() {
            return Err(());
        }
        let op = self.ops[self.next_op];
        let remaining = match op {
            Noop => 1,
            AddX(_) => 2,
        };
        self.current_op = Some(OpCycles{op, remaining});
        self.next_op += 1;
        Ok(())
    }

    fn do_op(&mut self) {
        let current_op = self.current_op.as_mut().unwrap();
        match current_op.op {
            Noop => {}
            AddX(v) => {self.x += v;}
        }
    }
}

impl fmt::Display for CPU {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "CPU(x={}, current_op={:?}, cycle={})", self.x, self.current_op, self.cycle)
    }
}

fn setup_cpu() -> CPU {
    let mut operations: Vec<Operation> = fs::read_to_string("data/day10.txt")
        .expect("Should be able to read the file")
        .split_terminator('\n')
        .map(|line| line.try_into().unwrap())
        .collect();

    // println!("{:?}", operations);

    let mut cpu = CPU::new();
    cpu.supply_operations(&mut operations);

    cpu
}

pub fn p1() {
    let mut cpu = setup_cpu();

    let mut total = 0;
    loop {
        // println!("{}", cpu);
        if cpu.cycle >= 20 && (cpu.cycle - 20) % 40 == 0 {
            if cpu.cycle > 220 {
                panic!("to big cycle");
            }
            total += (cpu.cycle as i32) * cpu.x;
            // println!("{}", cpu);
        }
        if cpu.tick().is_err() {
            break;
        }
    }

    println!("{}", total);
}

pub fn p2() {
    let mut cpu = setup_cpu();

    loop {
        let pixel = ((cpu.cycle - 1) % 40) as i32;
        if cpu.x - 1 == pixel || cpu.x == pixel || cpu.x + 1 == pixel {
            print!("#");
        } else {
            print!(".");
        }
        if cpu.cycle % 40 == 0 {
            println!();
        }
        if cpu.tick().is_err() {
            break;
        }
    }
}
