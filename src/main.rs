use advent2022::day18 as day;

fn main() {
    println!("Problem 1:");
    day::p1();
    println!("\nProblem 2:");
    day::p2();
}
